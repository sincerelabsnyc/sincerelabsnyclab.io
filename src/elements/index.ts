import P from "./P.astro";
import B from "./B.astro";
import Section from "./Section.astro";
import Link from "./Link.astro";
import Align from "./Align.astro";

export { B, P, Section, Link, Align };
