---
layout: "@layouts/MarkdownLayout.astro"
---

# Threat modeling

I hear you that you want to make the app secure, so that it can be a trusted source for your users. That makes sense! I wrote up something about threat modeling to help you decide on which security precautions to take

## Security is always about tradeoffs.

If you store all your data in a box under the sea, with no connection to the Internet, then no one can access it, but also, you can't access your data either. And the joke is, a hacker will get into it anyway.

## Planning for certain attacks is called "threat modeling"

What are we worried will happen? Let's use physical security as an example: We aren't worried that an attacker will walk up to our servers and delete our data: we will have continuous backups of the database, and we trust AWS to have good-enough physical security for our app. If someone cares enough to get past AWS's security guards, and find our exact server in the data center, well good job to them. That is to say, these scenarios are outside our threat model. We aren't making any precautions here.

## Basic threat model

What are the motives we are concerned about? Who would want to disrupt our voting data? And for what reason? For apps of our size, I think we're most worried about someone who might mess with us just cause they notice an obvious flaw, like for example the auth key stored in UserDefaults instead of the Keychain (which we are going to fix regardless). Perhaps you have further insight into who might want to mess with our data?
Stopping that group is where we should focus resources. And does FaceID/TouchID protect against that group?

## Where does FaceID/TouchID fit into our threat model?

So what is FaceID/TouchID protecting from? We are comparing to the user always being logged in.
To interact in the logged in app, a malicious user must first get past the phone's native FaceID/TouchID to unlock the device. And then find our app on the home screen, and then interact with the app in a way that doesn't reflect the main user's opinions.

If the phone was given to the malicious user in an unlocked state, then yes, a FaceID, TouchID, or PIN code prompt would help slow down a malicious user. Yes, only "slow down" and not "prevent", since yes, a dedicated malicious user could spoof the FaceID or a TouchID. I think most folks generally expect malicious users to give up when faced with FaceID.

If the phone was given to the malicious user in an unlocked state, and the app's FaceID was also unlocked, there isn't much we could do in that case. To protect against this example, we could require FaceID/TouchID/PIN code prompt every time someone taps on a button in the app, however, that might be too annoying for users. That might be too secure, to the detriment of usability, especially if users we attract don't care about that threat model.

## Conclusion

Adding FaceID/TouchID can be an investment. What is the impact on our security threat model?
