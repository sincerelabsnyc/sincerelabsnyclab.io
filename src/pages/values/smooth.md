---
layout: "@layouts/MarkdownLayout.astro"
---

## Smooth experiences

We love going to almost extreme measures when it's the right thing for the project.

When building the main map screen for [Underway NYC](https://underway.nyc), we started with a standard map component from Apple. However, this wasn't smooth enough: because when you're in a rush, the MTA's iconic map is more visually readable. How did we deliver the rich experience you see in the app?

**Displaying the MTA's map** was no small feat! For starters, it's published as a PDF; it's not a drop-in component. Displaying the map as a pure PDF directly was impossible, because of the sheer size of the map. It doesn't fit in memory. Using a custom-built rendering tool, we convert the PDF to HEIC format at different zoom levels, and then cut those zoom levels into images. Then we use a customized version of [ARTiledImageView](https://github.com/dblock/ARTiledImageView) on iOS and [MapView](https://github.com/p-lr/MapView) on Android to seamlessly display the tiled map. The libraries offer beautiful scrolling and zooming as well.

**Tapping the map** is another issue entirely. The MTA does not publish the coordinates for each station on the map. And sometimes, the MTA moves the locations of the stations between updates. To show the correct station when someone taps on a station circle, we found & placed a coordinate in our database by hand, for each of the 471 station circles on the map.

And the proof of the work is in the experience we delivered.

This is the kind of attention to detail you can expect when working with us.
