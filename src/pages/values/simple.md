---
layout: "@layouts/MarkdownLayout.astro"
---

## Simple

We built [Underway NYC](https://underway.nyc) to be simple. Simple to use, and simple to maintain.

**Simple to use** Tap on the map to see arriving trains. A simple premise. But a fresh premise. Other apps required the user to enter their current location and their destination. Other apps supported tapping on the map, but required several taps to see when trains would arrive.

I was demoing Underway NYC for a friend, and they exclaimed, "It's so simple!" Yes, that made me feel so good.

**Simple to maintain** Unlike many apps written to pad résumés, we stripped down the deployment of Underway. There is long-term storage for the critical path of the app. It's all stateless! If the server goes down, nothing is lost. [Terraform](https://terraform.io) allows us to easily provision new resources. And [Kubernetes](https://kubernetes.io) allows the resources to self-heal.

Since the app is only real-time data, any data older than 5 minutes is out-of-date anyway. Why bring in a database when it's unnecessary?

In addition, [Kubernetes](https://kubernetes.io) may be overkill for some apps, but for Underway, this community-maintained project was a perfect fit. It immediately reduced outages and immediately gave us more peace of mind, and a simplified codebase.

The solution you purchased should help you spend more time with customers. It shouldn't take you away from what you are trying to do.

This is the kind of attention to detail you can expect when working with us.
