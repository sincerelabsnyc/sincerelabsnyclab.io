import { defineConfig } from "astro/config";

import tailwind from "@astrojs/tailwind";

// https://astro.build/config
export default defineConfig({
  site: "https://sincerelabs.nyc",
  publicDir: "./static",
  outDir: "public",
  integrations: [tailwind()],
});
